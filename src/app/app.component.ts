import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { environment } from '../environments/environment';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  theme = environment.theme;
  www = environment.www;
  createUser = environment.createUser + '?callback=' + window.location;
  login = environment.login + '?callback=' + window.location;
  logout = environment.logout + '?callback=' + environment.www;
  profil = environment.profil + '?callback=' + window.location;
  home = environment.home;
  myRecords = environment.myRecords;
  datasources = environment.datasources;
  tab = environment.datasources[0].key;
  feedbackBaseUrl = environment.feedbackBaseUrl;
  userRoles: JSON;

  scripts = environment.feedbackBaseUrl ? [ {
    src: environment.feedbackBaseUrl + '/api/public/v1/feedback/widget.js',
    async: 'true'
  } ] : [];

  constructor(private userService:UserService, private sanitizer: DomSanitizer,private title: Title ) {
  }

  ngOnInit() {
    //this.userService.findUserRights().subscribe(userRoles => this.userRoles = userRoles);
    this.loadExternalScripts();
    this.loadFavicon();
    this.updateTitle();
  }

  loadExternalScripts() {
    let head = document.getElementsByTagName('head')[0];
    this.scripts.forEach(nfo => {
      let elm = document.createElement('script');
      elm.type = 'text/javascript';
      for (let key in nfo) {
        elm[key] = nfo[key];
      }
      head.appendChild(elm);
    });
  }

  loadFavicon() {
    let head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.type = 'image/x-icon';
    link.rel = 'shortcut icon';
    link.href = 'assets/' + this.theme + '/images/favicon.png';
    head.appendChild(link);
  }

  updateTitle() {
    switch(this.theme) {
      case 'guichet-partenaires' : this.title.setTitle('Guichet-Partenaires'); break;
      case 'guichet-qualifications' : this.title.setTitle('Guichet-Qualifications'); break;
      case 'guichet-entreprises' : 
      default :this.title.setTitle('Guichet-Entreprises'); break; 
    }
  }
}
