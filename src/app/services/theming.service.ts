import {Pipe, PipeTransform} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'
import { SafeResourceUrl } from '@angular/platform-browser';
import {environment} from '../../environments/environment';

@Pipe({
  name: 'theming'
})
export class Theming implements PipeTransform  {
  theme = this.whichTheme();

  constructor(private sanitizer: DomSanitizer){}

  transform(path: string) : SafeResourceUrl {
    console.debug('Apply theme for : assets/' + this.theme + '/'+ path);
    return this.sanitizer.bypassSecurityTrustResourceUrl('assets/' + this.theme + '/'+ path);
  }

  whichTheme() : String {
    let m = /.*(?:\.([^.]+))\.[^.]+$/ .exec(location.hostname);
    if (m) {
      return m[1];
    } else {
      return environment.theme;
    }
  }
}