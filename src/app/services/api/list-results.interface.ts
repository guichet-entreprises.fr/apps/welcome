import { Specification } from "../specifications.service";

export interface ListResult<T> {
    content: Specification[]
    maxResults: number
    startIndex: number
    totalResults:number
}