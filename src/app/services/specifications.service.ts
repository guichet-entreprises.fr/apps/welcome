import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/forkJoin';

import { ListResult } from './api/list-results.interface';
import {environment} from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from './user.service';

@Injectable()
export class SpecificationsService {

  constructor(protected http: Http, private userService:UserService) {}

  list(dsPrivateUrl: string = null, search: string = null, page: number = 1, limit: number = 9,filtre:string=null, useRoles: boolean = false, userRoles: JSON): Observable<ListResult<Specification>> {
    let myParams = new URLSearchParams();
    var startIndex = (page - 1) * limit;
    var maxResult = limit;
    if (search) myParams.set('q', search)
    if (page) myParams.set('startIndex', String(startIndex))
    if (limit) myParams.set('maxResults', String(maxResult))
    if (filtre) myParams.set('filters',"groupe:"+filtre)
    
    myParams.append('orders',"priority:desc")
    myParams.append('orders',"reference:asc")

    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    if (useRoles) {
      return this.userService.findUserRights().switchMap(rights => {
        myHeaders.append('X-Roles', JSON.stringify(rights));
        return this.http.get(dsPrivateUrl + '/v1/Specification/search', new RequestOptions({ headers: myHeaders, params: myParams })).map(res => res.json());
      });
    }
    return this.http.get(dsPrivateUrl + '/v1/Specification/search', new RequestOptions({ headers: myHeaders, params: myParams })).map(res => res.json());
  }

  transform(collection: any, property: string): any {
    // prevents the application from breaking if the array of objects doesn't exist yet
    if (!collection) {
      return null;
    }

    const groupedCollection = collection.reduce((previous, current) => {
      if (!previous[current[property]]) {
        previous[current[property]] = [current];
      } else {
        previous[current[property]].push(current);
      }

      return previous;
    }, {});

    // this will return an array of objects, each object containing a group of objects
    return Object.keys(groupedCollection).map(key => ({ key, value: groupedCollection[key] }));
  }
}

export class Specification {
  constructor(public author: string,
    public title: string) {
  }
}