import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor(protected http: Http, private CookieService: CookieService) { }
  
  findUserRights(): Observable<JSON> {
    return this.http.get(window.location.href).map(
      (response : Response) => {
        return response.headers.get('X-GE-ID');
      }
    )
    .switchMap(userIdentifier => {
      let myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');
      return this.http.get(environment.directoryWsUrl + '/public/services/authority/user/' + userIdentifier +'/rights', new RequestOptions({ headers: myHeaders })).map(res => res.json()); 
    });
  }
}