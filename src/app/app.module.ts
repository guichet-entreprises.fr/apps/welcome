import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';


import { AppComponent } from './app.component';
import { SpecificationsComponent } from './components/specifications/specifications.component';
import { SpecificationsService } from './services/specifications.service';
import { UserService } from './services/user.service';
import { PaginationComponent } from './components/pagination/pagination.component';
import { GroupByPipe } from './services/api/cutom.pipe';
import { CookieService } from 'ngx-cookie-service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Theming } from './services/theming.service';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    SpecificationsComponent,
    PaginationComponent,
    GroupByPipe,
    Theming,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFontAwesomeModule
  ],
  providers: [SpecificationsService, UserService, CookieService],
  bootstrap: [AppComponent, HeaderComponent]
})
export class AppModule { }
