var themeOptions = {
    apply: true,
    publicResourcesBaseUrl: 'assets'
};
var baseUrl = /*[[@{/}]]*/ '/';
var locale = {
    language: 'fr',
    country: 'FR',
    full: 'fr_FR'
};
