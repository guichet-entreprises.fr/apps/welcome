# Changelog
Ce fichier contient les modifications techniques du module.

- Projet : [Welcome](https://tools.projet-ge.fr/gitlab/apps/nash-welcome)

## [2.12.4.0] - 2019-10-06

### Ajout

### Modification

__Properties__ :

Concerne uniquement les applications : welcome-ge-ui et welcome-gp-ui

| fichier   |description |
|-----------|:-------------:|
| welcome-conf.json | Ajout de l'URL publique du composant FEEDBACK (feedbackBaseUrl) |

### Suppression

- Suppression du "Ribbon"


## [2.12.2.0] - 2019-10-04

###  Ajout

MINE-996 : Modifier les URLs de callback de la bulle Welcome

### Modification

__Properties__ :

Concerne uniquement les applications : welcome-ge-ui et welcome-gp-ui

| fichier   |description |
|-----------|:-------------:|
| welcome-conf.json | Ajout de l'URL publique du WWW |

### Suppression

## [2.11.10.1] - 2019-09-06

###  Ajout

MINE-913 : Présenter les services offerts aux partenaires et au support

### Modification

### Suppression


