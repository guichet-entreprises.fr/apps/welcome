#Welcome Guichet-Partenaires : Dossier d'installation
===================================


## Composants de la bulle welcome

###welcome

IHM public

-   nom de projet : nash-welcome

-   URL : https://welcome.${env}.guichet-partenaires.fr

-   bulle : welcome

| **GroupID**          	| **Nom du fichier**                 | **Description** |
|-----------------------|------------------------------------|-----------------|
| fr.ge.common.welcome 	| nash-welcome-*x.y.z.tar.gz   | l'ensemble des fichiers statiques de l'application

* *x.y.z* représente la version livrée.

###Configuration du socle logiciel

Doivent être provisionnés sur les serveurs les composants suivant :

| **Noeud applicatif** 	| **Description**              | **Composants techniques requis** |
|-----------------------|------------------------------|----------------------------------|
| WELCOME_VH        	| Frontal du module applicatif | Apache                           |            |


###Installation du virtual host

| **Nom**  	| **Description**           | **&lt; PROD**                         | **PROD**                    		|
|-----------|---------------------------|---------------------------------------|----------------
| Welcome 	| URL de l'instance interne | welcome.${env}.guichet-partenaires.fr |welcome.guichet-partenaires.fr 	|


Pour l'exemple, dans le répertoire **&nbsp;/etc/apache2/sites-enabled**, ajouter un virtualhost (ie **XX-welcome.${env}.guichet-partenaires.fr.conf**)


```xml
	#********************************************
	# Vhost template in module puppetlabs-apache
	# Managed by Puppet
	#********************************************
	
	<VirtualHost *:80>
		ServerName welcome.${env}.guichet-partenaires.fr
		## Vhost docroot
		DocumentRoot "/srv/www/welcome.${env}.guichet-partenaires.fr"

		<Location "/">
		Require all granted
		</Location>

		## Logging
		ErrorLog "/var/log/apache2/welcome.${env}.guichet-partenaires.fr_error.log"
		ServerSignature Off
		CustomLog "/var/log/apache2/welcome.${env}.guichet-partenaires.fr_access.log" "forwarded_host"

		## Header rules
		## as per http://httpd.apache.org/docs/2.2/mod/mod_headers.html#header
		Header always set X-Content-Type-Options nosniff
		Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"
		Header always set X-XSS-Protection "1; mode=block"
		Header always set Content-Security-Policy "default-src https: 'unsafe-eval' 'unsafe-inline'; font-src 'self' data:; object-src data: 'self'; frame-ancestors 'self'; img-src data: 'self'; style-src data: 'self' 'unsafe-inline'"
		Header always set X-Frame-Options SAMEORIGIN
		Header always set Referrer-Policy same-origin
		
		## Proxy rules
		ProxyRequests Off
		ProxyPreserveHost Off
		ProxyPass /nashbo/api/ http://backoffice-forms-ws.${env}.guichet-partenaires.loc/api/
		ProxyPassReverse /nashbo/api/ /api/
		ProxyPass /nashdirectory/api/ http://forms-directory-ws.${env}.guichet-partenaires.loc/api/
		ProxyPassReverse /nashdirectory/api/ /api/
		ProxyPass /nashsupport/api/ http://nashsupport-ws.dev.guichet-partenaires.loc/api/
		ProxyPassReverse /nashsupport/api/ /api/
		ProxyPass /directory/api/ http://directory-ws.${env}.guichet-partenaires.loc/api/
		ProxyPassReverse /directory/api/ /api/
	</VirtualHost>
```

###fichier welcome-conf.json

```javascript
{
  "production": true,
  "createUser": "https://account.${env}.guichet-partenaires.fr/users/new",
  "login": "https://account.${env}.guichet-partenaires.fr",
  "profil": "https://account.${env}.guichet-partenaires.fr/users/modify",
  "logout": "https://account.${env}.guichet-partenaires.fr/disconnect",
  "home": "https://www.${env}.guichet-partenaires.fr",
  "myRecords": "https://backoffice-dashboard.${env}.guichet-partenaires.fr",
  "www": "https://www.${env}.guichet-partenaires.fr",
  "cookieName" : "sessionidge",
  "directoryWsUrl": "https://welcome.${env}.guichet-partenaires.fr/directory/api",
  "feedbackBaseUrl": "https://feedback-ws.${env}.guichet-entreprises.fr",
  "theme": "guichet-partenaires",
  "datasources" : [
    {
      "key" : "bo",
      "title" : "Accéder à mes services",
      "checkRoles" : true,
      "privateUrl" : "https://welcome.${env}.guichet-partenaires.fr/nashbo/api",
      "publicUrl" : "https://backoffice-forms.${env}.guichet-partenaires.fr"
    },
    {
      "key" : "support",
      "title" : "Réservé GE",
      "checkRoles" : true,
      "privateUrl" : "https://welcome.${env}.guichet-partenaires.fr/nashsupport/api",
      "publicUrl" : "https://support-nash.${env}.guichet-partenaires.fr"
    },
    {
      "key" : "directory",
      "title" : "Configurer mes services",
      "checkRoles" : true,
      "privateUrl" : "https://welcome.${env}.guichet-partenaires.fr/nashdirectory/api",
      "publicUrl" : "https://forms-directory.${env}.guichet-partenaires.fr"
    }
  ]
}
```
