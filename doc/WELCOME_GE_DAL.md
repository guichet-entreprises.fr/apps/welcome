
# Welcome Guichet-Entreprises: Dossier d'Architecture Logiciel
============================================



## Description des exigences

### Exigences métiers

*Le projet « welcome» permet de récupérer les formalités de n'importe que Nash.
*Le paramétrage des sources des données (nash) ce fait via des properties.
*Les formalités s'affichent selon les droits des l'utilisateurs connectés.

### Contrainte de performance
 Sans objet

### Contrainte de de disponibilité

Interruptions de services prévisibles : installation, mise à jour applicative.

Aucune contrainte de disponibilité n'est définie.

### Contrainte de volumétrie


### ArchiLogique Schéma de l'architecture logique
![Schéma de l'architecture logique](doc/media/schema_architecture_logique_welcome_ge.png)


## Architecture logicielle

### Décomposition en sous-systèmes applicatifs

**Description des nœuds applicatifs et des services que chacun rend.**


| **type de noeud applicatif** | **nom de noeud applicatif** | **description**                                                              	|
|------------------------------|-----------------------------|----------------------------------------------------------------------------------|
| apache_vhost                 | WELCOME_VH                | virtual host Apache qui sert index.html                                 		|
                                            		|

**Explication des types de noeuds applicatifs : **

| **type de noeud applicatif** | **description**  |
|------------------------------|------------------|
| apache_vhost  | un virtual host qui contient les fichiers html js et css

**Description macroscopique des flux internes**

**Liste des flux internes au SI ou au VLAN, à choisir **

| **Origine**   | **Destination** 	| **Protocol** | **Port** |
|---------------|-------------------|--------------|----------|


**Cinématique d'alimentation**

Sans objet.

**Description macroscopique des flux externes**

| **Origine**   | **Destination** 	    | **Protocol** 	| **Port** 	|
|---------------|-----------------------|---------------|-----------|
| Internet		| WELCOME_VH		    | HTTPS         | 80     	|
| Internet	    | NASHWEB_WS_VH   	    | HTTPS         | 80     	|
| Internet	    | NASHPROFILER_WS_VH   	| HTTPS         | 80     	|


**Eléments d'exploitation**

Sans objet.
