#Welcome Guichet-Entreprises: Dossier d'installation
===================================

##Composants de la bulle welcome

IHM public

-   nom de projet : nash-welcome

-   URL : https://welcome.${env}.guichet-entreprises.fr

-   bulle : welcome

| **GroupID**          	| **Nom du fichier**                 | **Description** |
|-----------------------|------------------------------------|-----------------|
| fr.ge.common.welcome 	| nash-welcome-*x.y.z.tar.gz   | l'ensemble des fichiers statiques de l'application

* *x.y.z* représente la version livrée.

##Configuration du socle logiciel

Doivent être provisionnés sur les serveurs les composants suivant :

| **Noeud applicatif** 	| **Description**              | **Composants techniques requis** |
|-----------------------|------------------------------|----------------------------------|
| WELCOME_VH        	| Frontal du module applicatif | Apache                           |            |


### Installation du virtual host

| **Nom**  	| **Description**           | **&lt; PROD**                         | **PROD**                    		|
|-----------|---------------------------|---------------------------------------|----------------
| Welcome 	| URL de l'instance interne | welcome.${env}.guichet-entreprises.fr |welcome.guichet-entreprises.fr 	|


Pour l'exemple, dans le répertoire **&nbsp;/etc/apache2/sites-enabled**, ajouter un virtualhost (ie **XX-welcome.${env}.guichet-entreprises.fr.conf**)


```xml
	#********************************************
	# Vhost template in module puppetlabs-apache
	# Managed by Puppet
	#********************************************
	
	<VirtualHost *:80>
		ServerName welcome.${env}.guichet-entreprises.fr
		## Vhost docroot
		DocumentRoot "/srv/www/welcome.${env}.guichet-entreprises.fr"

		<Location "/">
		Require all granted
		</Location>

		## Logging
		ErrorLog "/var/log/apache2/welcome.${env}.guichet-entreprises.fr_error.log"
		ServerSignature Off
		CustomLog "/var/log/apache2/welcome.${env}.guichet-entreprises.fr_access.log" "forwarded_host"

		## Header rules
		## as per http://httpd.apache.org/docs/2.2/mod/mod_headers.html#header
		Header always set X-Content-Type-Options nosniff
		Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"
		Header always set X-XSS-Protection "1; mode=block"
		Header always set Content-Security-Policy "default-src https: 'unsafe-eval' 'unsafe-inline'; font-src 'self' data:; object-src data: 'self'; frame-ancestors 'self'; img-src data: 'self'; style-src data: 'self' 'unsafe-inline'"
		Header always set X-Frame-Options SAMEORIGIN
		Header always set Referrer-Policy same-origin
		
		## Proxy rules
		ProxyRequests Off
		ProxyPreserveHost Off
		ProxyPass /nashweb/api/ http://nashweb-ws.${env}.guichet-qualifications.loc/api/
		ProxyPassReverse /nashweb/api/ /api/
		ProxyPass /nashprofiler/api/ http://profiler-ws.${env}.guichet-partenaires.loc/api/
		ProxyPassReverse /nashprofiler/api/ /api/
		ProxyPass /directory/api/ http://directory-ws.${env}.guichet-partenaires.loc/api/
		ProxyPassReverse /directory/api/ /api/
	</VirtualHost>
```

###fichier welcome-conf.json

```javascript
{
  "production": true,
  "createUser": "https://account.${env}.guichet-entreprises.fr/users/new",
  "login": "https://account.${env}.guichet-entreprises.fr",
  "profil": "https://account.${env}.guichet-entreprises.fr/users/modify",
  "logout": "https://account.${env}.guichet-entreprises.fr/disconnect",
  "home": "https://www.${env}.guichet-entreprises.fr/fr",
  "myRecords": "https://dashboard.${env}.guichet-entreprises.fr",
  "www": "https://www.${env}.guichet-entreprises.fr",
  "cookieName" : "sessionidge",
  "directoryWsUrl": "https://welcome.${env}.guichet-entreprises.fr/directory/api",
  "feedbackBaseUrl": "https://feedback-ws.${env}.guichet-entreprises.fr",
  "theme": "guichet-entreprises",
  "datasources" : [
    {
      "key" : "profiler",
      "title" : "Trouver ma formalité",
      "checkRoles" : false,
      "privateUrl" : "https://welcome.${env}.guichet-entreprises.fr/nashprofiler/api",
      "publicUrl" : "https://profiler.${env}.guichet-entreprises.fr"
    },
    {
      "key" : "forms2",
      "title" : "Engager ma démarche",
      "checkRoles" : false,
      "privateUrl" : "https://welcome.${env}.guichet-entreprises.fr/nashweb/api",
      "publicUrl" : "https://forms2.${env}.guichet-entreprises.fr"
    }
  ]
}
```
